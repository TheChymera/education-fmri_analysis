from samri.plotting import timeseries

events_file='/usr/share/samri_bidsdata/preprocessing/generic/sub-4007/ses-ofM/func/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-1_events.tsv'

ax = timeseries.roi_based(
	      events_filename=events_file,
	      flip=True,
	      #ts_filename=ts_file,
	      plot_design_regressors=[0],
	      )
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.yaxis.label.set_color('0.7')
