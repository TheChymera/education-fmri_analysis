import samri.plotting.maps as maps
from os import path

stat_map = '/usr/share/samri_bidsdata/l1/generic/sub-4007/ses-ofM/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-1_cbv_tstat.nii.gz'
template = str(path.abspath(path.expanduser('/usr/share/mouse-brain-atlases/dsurqec_40micron_masked.nii')))
maps.stat(stat_maps=[stat_map],
        template=template,
        cut_coords=[(-3,-1.2,-1.6)],
        annotate=True,
        scale=0.5,
        show_plot=False,
        interpolation=None,
        draw_colorbar=True,
        black_bg=False,
        threshold=3,
        )
