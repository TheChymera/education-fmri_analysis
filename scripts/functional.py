from samri.plotting.maps import contour_slices

anat = '/usr/share/samri_bidsdata/bids_collapsed/sub-4007/ses-ofM/func/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-0_bold.nii.gz'

contour_slices(anat,
	alpha=[0.01],
	figure_title='',
	file_template=anat,
	#force_reverse_slice_order=False,
	legend_template='',
	substitutions=[{1:2},],
	levels_percentile=[50],
	ratio=[3,5],
	slice_spacing=0.65,
	save_as='lala.png',
	style='',
	)
