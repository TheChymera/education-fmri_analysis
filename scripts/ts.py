from samri.plotting import timeseries
from samri.fetch.local import roi_from_atlaslabel

beta_file='/usr/share/samri_bidsdata/l1/generic/sub-4007/ses-ofM/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-1_cbv_cope.nii.gz'
design_file='/usr/share/samri_bidsdata/l1/generic/sub-4007/ses-ofM/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-1_cbv_design.mat'
ts_file='/usr/share/samri_bidsdata/l1/generic/sub-4007/ses-ofM/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-1_cbv_maths_filt.nii.gz'

mapping='/usr/share/mouse-brain-atlases/dsurqe_labels.csv'
atlas='/usr/share/mouse-brain-atlases/dsurqec_40micron_labels.nii'
my_roi = roi_from_atlaslabel(atlas,
	mapping=mapping,
	label_names=['cortex'],
	)

ax = timeseries.roi_based(
		ts_filename=ts_file,
		flip=True,
		roi=my_roi,
		)
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.yaxis.label.set_color('0.7')
