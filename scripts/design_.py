from samri.fetch.local import roi_from_atlaslabel
from samri.plotting import timeseries

beta_file='/usr/share/samri_bidsdata/l1/generic/sub-4007/ses-ofM/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-1_cbv_cope.nii.gz'
design_file='/usr/share/samri_bidsdata/l1/generic/sub-4007/ses-ofM/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-1_cbv_design.mat'

mapping='/usr/share/mouse-brain-atlases/dsurqe_labels.csv'
atlas='/usr/share/mouse-brain-atlases/dsurqec_40micron_labels.nii'
my_roi = roi_from_atlaslabel(atlas,
	mapping=mapping,
	label_names=['cortex'],
	)

ax = timeseries.roi_based(
	      beta_filename=beta_file,
	      flip=True,
	      #ts_filename=ts_file,
	      design_filename=design_file,
	      roi=my_roi,
	      plot_design_regressors=[1],
	      )
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.yaxis.label.set_color('0.7')
