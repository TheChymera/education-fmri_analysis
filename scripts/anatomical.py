from samri.plotting.maps import contour_slices

anat = '/usr/share/samri_bidsdata/bids/sub-4007/ses-ofM/anat/sub-4007_ses-ofM_acq-TurboRARElowcov_T2w.nii.gz'

contour_slices(anat,
	alpha=[0.01],
	figure_title='',
	file_template=anat,
	#force_reverse_slice_order=False,
	legend_template='',
	substitutions=[{1:2},],
	levels_percentile=[50],
	ratio=[3,5],
	slice_spacing=0.65,
	save_as='lala.png',
	style='',
	)
