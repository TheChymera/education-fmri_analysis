\input{slides/header.tex}
\title[Data Analysis in fMRI --- exemplified with small animal data]{Data Analysis in fMRI}
\subtitle{exemplified with small animal data\\\href{https://bitbucket.com/TheChymera/education-fmri_analysis}{\small\texttt{[ bitbucket.com/TheChymera/education-fmri\_analysis ]}}}
\author[Horea Christian]{Horea Christian\\\href{http://chymera.eu}{\small\texttt{[ chymera.eu ]}}}
\institute{Institute for Biomedical Engineering, ETH and University of Zürich}
\begin{document}
	\begin{frame}
		\titlepage
	\end{frame}
	\section{Fundamentals}
		\subsection{Why is analysis important and what does it entail?}
			\begin{frame}{But Why?}
				\begin{center}
						In $x$, I just “look at the data”, why not in fMRI?
				\end{center}
				\begin{itemize}
					\item \textcolor{lg}{Ideally you don't just “look at the data” in $x$ either.}
					\item Our data is 4D.
					\item We're dealing with low effect amplitudes (method and feature).
					\item Complex systems require complex analysis.
				\end{itemize}
			\end{frame}
			\begin{frame}{Broader Workflow Context}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/metadata}
				\end{figure}
				\begin{itemize}
					\item \textbf{Bruker}: Vendor of hardware and proprietary acquisition software.
					\item \textbf{SAMRI}: Small Animal MRI, FOSS data analysis suite developed at the ETH/UZH.
				\end{itemize}
			\end{frame}
	\section{Data}
		\subsection{What you're working with.}
			\begin{frame}{The NIfTI Volumetric Format $\rightarrow$ \href{https://nifti.nimh.nih.gov/nifti-1/}{\normalsize nifti.nimh.nih.gov/nifti-1}}
				“Neuroimaging Informatics Technology Initiative” format, consisting of:
				\begin{itemize}
					\item A data matrix.
					\item A minimal header (incl. affine).
				\end{itemize}
				\vspace{.5em}
				\begin{figure}
					\centering\hfill
					\includegraphics[width=0.28\textwidth]{img/matrix_points.png}\hfill
					\includegraphics[width=0.31\textwidth]{img/matrix_volumetric.png}\hfill\hfill 
					\caption{Data matrix, shown as points or (incorrectly) as volumes.}
				\end{figure}
			\end{frame}
			\begin{frame}{The Affine Transform}
				\begin{itemize}
					\item is a simple (in 3D $3x4$) matrix,
					\item without which the data is spatially ambiguous.
				\end{itemize}
				\begin{figure}
					\centering
					\includegraphics[width=0.85\textwidth]{img/affine.png}
					\caption{Affine transformation matrices applied to a unit square. Adapted from \href{https://commons.wikimedia.org/wiki/User:Cmglee}{Cmglee} (\href{https://creativecommons.org/licenses/by-sa/3.0/deed.en}{CC-BY-SA})}
				\end{figure}
			\end{frame}
			\begin{frame}{Metadata}
				\begin{multicols}{2}
					\begin{itemize}
						\item Vital to experimental evaluation.
						\item Not entirely stored in the NIfTI header.
					\end{itemize}
				\end{multicols}
				\vspace{-1em}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/metadata_bad}
					\vspace{-2em}
					\caption{Workflow example of bad metadata management.}
				\end{figure}
			\end{frame}
			\begin{frame}{The BIDS Standard $\rightarrow$ \href{http://bids.neuroimaging.io/bids\_spec1.0.2.pdf}{\normalsize bids.neuroimaging.io/bids\_spec1.0.2.pdf}}
			“Brain Imaging Data Structure”, a peer-reviewed way to name your files:\\
			\vspace{-.2em}
				\small{\centerline{\texttt{$<$base$>$/\textcolor{myred}{sub-$<$sub$>$}/\textcolor{mygreen}{ses-$<$ses$>$}/func/}}}\\
				\small{\centerline{\texttt{\textcolor{myred}{sub-$<$sub$>$}\_\textcolor{mygreen}{ses-$<$ses$>$}\_\textcolor{myblue}{task-$<$task$>$}\_\textcolor{myyellow}{acq-$<$acq$>$}\_\textcolor{mycyan}{run-$<$run$>$}\_\textcolor{mymagenta}{$<$modality$>$}.nii}}}

			\vspace{1.6em}

				\normalsize Example NIfTI file:\\
				\small{\centerline{\texttt{preprocessed/\textcolor{myred}{sub-5686}/\textcolor{mygreen}{ses-ofM}/func/}}}
				\small{\centerline{\texttt{\textcolor{myred}{sub-4007}\_\textcolor{mygreen}{ses-ofM}\_\textcolor{myblue}{task-JogB}\_\textcolor{myyellow}{acq-seEPI}\_\textcolor{mycyan}{run-0}\_\textcolor{mymagenta}{bold}.nii}}}
			\vspace{.4em}

				\normalsize Suplementary files:\\
				\small{\centerline{\texttt{preprocessed/\textcolor{myred}{sub-5686}/\textcolor{mygreen}{ses-ofM}/func/}}}
				\small{\centerline{\texttt{\textcolor{myred}{sub-4007}\_\textcolor{mygreen}{ses-ofM}\_\textcolor{myblue}{task-JogB}\_\textcolor{myyellow}{acq-seEPI}\_\textcolor{mycyan}{run-0}\_\textcolor{mymagenta}{bold}.json}}}
			\vspace{-.4em}

				\small{\centerline{\texttt{preprocessed/\textcolor{myred}{sub-5686}/\textcolor{mygreen}{ses-ofM}/func/}}}
				\small{\centerline{\texttt{\textcolor{myred}{sub-4007}\_\textcolor{mygreen}{ses-ofM}\_\textcolor{myblue}{task-JogB}\_\textcolor{myyellow}{acq-seEPI}\_\textcolor{mycyan}{run-0}\_\textcolor{mymagenta}{events}.tsv}}}
			\end{frame}
			\begin{frame}{Data Example --- Functional}
				\vspace{-1.5em}
				\py{pytex_fig('scripts/functional.py', conf='slides/slices.conf')}
			\end{frame}
			\begin{frame}{Data Example --- Anatomical}
				\vspace{-1.5em}
				\py{pytex_fig('scripts/anatomical.py', conf='slides/slices.conf')}
			\end{frame}
			\begin{frame}{Data Example --- Template}
				\py{pytex_fig('scripts/dsurqec.py', conf='slides/template.conf')}
			\end{frame}
	\section{Preprocessing}
		\subsection{Data analysis requires more than just one step.}
			\begin{frame}{Is your data ready to answer your questions?}
				Depending on the question, you may need to:
				\begin{itemize}
					\item Correct the timing of your slices.
					\item Correct for motion in your data.
					\item Co-register your data (within your sample or to a population template).
					\item Smooth your data.
					\item Filter temporal or spatial frequencies.
					\item etc.
					\item \textcolor{lg}{But every “correction”} comes at a cost.
				\end{itemize}
			\end{frame}
			\begin{frame}{Workflow Example}
				\vspace{-3.8em}
				\begin{figure}
					\centering
					\includedot[width=0.47\textwidth]{data/preprocessing}
				\end{figure}
			\end{frame}
			\begin{frame}{Dummy Scans}
				\py{pytex_fig('scripts/dummy.py', conf='slides/template.conf')}
			\end{frame}
			\begin{frame}[fragile]{Registration}
				\begin{center}
				\begin{minipage}{.48\textwidth}
					\centering Structural to Template (Rigid)
				\begin{minted}[fontsize=\tiny, bgcolor=tlg]{python}
"s_rigid":{
	"transforms":"Rigid",
	"transform_parameters":(0.1,),
	"number_of_iterations":[2000,3000],
	"metric":"GC",
	"metric_weight":1,
	"radius_or_number_of_bins":64,
	"sampling_strategy":"Regular",
	"sampling_percentage":0.2,
	"convergence_threshold":1.e-16,
	"convergence_window_size":30,
	"smoothing_sigmas":[1,0],
	"sigma_units":"vox",
	"shrink_factors":[2,1],
	"use_estimate_learning_rate_once":False,
	"use_histogram_matching":True,
	},
				\end{minted}
				\end{minipage}
				\begin{minipage}{.48\textwidth}
					\centering Structural to Template (Affine)
				\begin{minted}[fontsize=\tiny, bgcolor=tlg]{python}
"affine":{
	"transforms":"Affine",
	"transform_parameters":(0.1,),
	"number_of_iterations":[500,250],
	"metric":"MI",
	"metric_weight":1,
	"radius_or_number_of_bins":16,
	"sampling_strategy":None,
	"sampling_percentage":0.3,
	"convergence_threshold":1.e-32,
	"convergence_window_size":30,
	"smoothing_sigmas":[1,0],
	"sigma_units":"vox",
	"shrink_factors":[1,1],
	"use_estimate_learning_rate_once":False,
	"use_histogram_matching":True,
	},
				\end{minted}
				\end{minipage}
				\end{center}
			\end{frame}
			\begin{frame}[fragile]{Registration}
				\begin{center}
				\begin{minipage}{.48\textwidth}
					\centering Structural to Template (Nonlinear)
				\begin{minted}[fontsize=\tiny, bgcolor=tlg]{python}
"syn":{
	"transforms":"SyN",
	"transform_parameters":(0.1, 2.0, 0.2),
	"number_of_iterations":[500,250],
	"metric":"MI",
	"metric_weight":1,
	"radius_or_number_of_bins":16,
	"sampling_strategy":None,
	"sampling_percentage":0.3,
	"convergence_threshold":1.e-32,
	"convergence_window_size":30,
	"smoothing_sigmas":[1,0],
	"sigma_units":"vox",
	"shrink_factors":[1,1],
	"use_estimate_learning_rate_once":False,
	"use_histogram_matching":True,
	},
				\end{minted}
				\end{minipage}
				\begin{minipage}{.48\textwidth}
					\centering Functional to Structural (Rigid)
				\begin{minted}[fontsize=\tiny, bgcolor=tlg]{python}
"f_rigid":{
	"transforms":"Rigid",
	"transform_parameters":(0.1,),
	"number_of_iterations":[300],
	"metric":"MI",
	"metric_weight":1,
	"radius_or_number_of_bins":32,
	"sampling_strategy":"Regular",
	"sampling_percentage":0.5,
	"convergence_threshold":1.e-8,
	"convergence_window_size":10,
	"smoothing_sigmas":[0],
	"sigma_units":"vox",
	"shrink_factors":[1],
	"use_estimate_learning_rate_once":False,
	"use_histogram_matching":False,
	},
				\end{minted}
				\end{minipage}
				\end{center}
			\end{frame}
	\section{GLM}
		\subsection{Determine the uncertainty of your interpretation of the data.}
			\begin{frame}{The General Expression}
				\centering \bm{$Y = XB + U$}
				\vspace{0.7cm}
				\begin{itemize}
					\item \bm{$Y$} = measurement matrix
					\item \bm{$X$} = (usually) design matrix
					\item \bm{$B$} = (usually) parameters to be estimated
					\item \bm{$U$} = error terms
				\end{itemize}
			\end{frame}
			\begin{frame}{A Simple Example}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					0&1\\
					0&1\\
					0&1\\
					1&0\\
					1&0\\
					1&0
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\mu_1\\
					\mu_2\\
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
			\end{frame}
			\begin{frame}{One-Way ANOVA}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					1&0&0\\
					1&0&0\\
					0&1&0\\
					0&1&0\\
					0&0&1\\
					0&0&1
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\mu_1\\
					\mu_2\\
					\mu_3\\
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{0.6cm}\\
				\centering $F = \frac{\text{variance between groups}}{\text{variance within groups}}$
				\vspace{0.3cm}\\
				Ombibus test for $H_0$: \textit{“The population means are equal.”}
			\end{frame}
			\begin{frame}{Two-Way ANOVA}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					\textcolor{blue}{1}&0&0&\textcolor{red}{1}&0\\
					\textcolor{blue}{1}&0&0&0&\textcolor{red}{1}\\
					0&\textcolor{blue}{1}&0&\textcolor{red}{1}&0\\
					0&\textcolor{blue}{1}&0&0&\textcolor{red}{1}\\
					0&0&\textcolor{blue}{1}&\textcolor{red}{1}&0\\
					0&0&\textcolor{blue}{1}&0&\textcolor{red}{1}
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\alpha_1\\
					\alpha_2\\
					\alpha_3\\
					\beta_1\\
					\beta_2
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{1cm}\\
				\centering No interactions!
			\end{frame}
			\begin{frame}{Two-Way ANOVA}
				\scriptsize$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					\textcolor{blue}{1}&0&0&\textcolor{red}{1}&0&\textcolor{violet}{1}&0&0&0&0&0\\
					\textcolor{blue}{1}&0&0&0&\textcolor{red}{1}&0&\textcolor{violet}{1}&0&0&0&0\\
					0&\textcolor{blue}{1}&0&\textcolor{red}{1}&0&0&0&\textcolor{violet}{1}&0&0&0\\
					0&\textcolor{blue}{1}&0&0&\textcolor{red}{1}&0&0&0&\textcolor{violet}{1}&0&0\\
					0&0&\textcolor{blue}{1}&\textcolor{red}{1}&0&0&0&0&0&\textcolor{violet}{1}&0\\
					0&0&\textcolor{blue}{1}&0&\textcolor{red}{1}&0&0&0&0&0&\textcolor{violet}{1}
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\alpha_1\\
					\alpha_2\\
					\alpha_3\\
					\beta_1\\
					\beta_2\\
					\gamma_{11}\\
					\gamma_{12}\\
					\gamma_{21}\\
					\gamma_{22}\\
					\gamma_{31}\\
					\gamma_{32}\\
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{1cm}\\
				\centering With interactions...
			\end{frame}
			\begin{frame}{Simple Regression}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					1&x_1\\
					1&x_2\\
					1&x_3\\
					1&x_4\\
					1&x_5\\
					1&x_6
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\beta_0\\
					\beta_1
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{0.3cm}\\
				Per-measurement expression: $y_i = \beta_0 + \beta_1 x_i + \epsilon_i$
			\end{frame}
			\begin{frame}{Multiple Regression}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					1&x_1&w_1\\
					1&x_2&w_2\\
					1&x_3&w_3\\
					1&x_4&w_4\\
					1&x_5&w_5\\
					1&x_6&w_6
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\beta_0\\
					\beta_1\\
					\beta_2
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{0.3cm}\\
				Per-measurement expression: $y_i = \beta_0 + \beta_1 x_i + \beta_2 w_i + \epsilon_i$
			\end{frame}
	\section{fMRI Modelling}
		\subsection{Apply the GLM in an fMRI context.}
			\begin{frame}{Workflow Example}
				\vspace{-0.8em}
				\begin{figure}
					\centering
					\includedot[width=0.9\textwidth]{data/glm}
				\end{figure}
			\end{frame}
			\begin{frame}{SPM --- Statistical Parametric Mapping}
				\begin{itemize}
					\item \textbf{Statistical approach} (but also a software package)
					\item Voxel-wise time-series analysis
					\item Mass-univariate analysis
					\item Adressing specific timeseries-issues such as autocorrelation.
				\end{itemize}
			\end{frame}
			\begin{frame}{Single Voxel Regression Model - Basic Design}
				\vspace{0.52em}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.15\textwidth}
						\py{pytex_fig('scripts/ts.py', conf='slides/glm.conf')}
					\end{minipage}
					$= \beta_0 +$
					\begin{minipage}{.15\textwidth}
						\py{pytex_fig('scripts/events.py', conf='slides/glm.conf')}
					\end{minipage}
					$\times \beta_1 + \epsilon$
				}
			\end{frame}
			\begin{frame}{Single Voxel Regression Model - Convolution}
				\vspace{0.52em}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.15\textwidth}
						\py{pytex_fig('scripts/ts.py', conf='slides/glm.conf')}
					\end{minipage}
					$= \beta_0 +$
					\begin{minipage}{.15\textwidth}
						\py{pytex_fig('scripts/design.py', conf='slides/glm.conf')}
					\end{minipage}
					$\times \beta_1 + \epsilon$
				}
			\end{frame}
			\begin{frame}{Single Voxel Regression Model - Convolution, Multiple Regression}
				\vspace{0.52em}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.15\textwidth}
						\py{pytex_fig('scripts/ts.py', conf='slides/glm.conf')}
					\end{minipage}
					$= \beta_0 +$
					\begin{minipage}{.15\textwidth}
						\py{pytex_fig('scripts/design.py', conf='slides/glm.conf')}
					\end{minipage}
					$\times \beta_1 +$
					\begin{minipage}{.15\textwidth}
						\py{pytex_fig('scripts/design_.py', conf='slides/glm.conf')}
					\end{minipage}
					$\times \beta_2 + \epsilon$
				}
			\end{frame}
			\begin{frame}{Statistic Map --- centered at stimulation site}
				\vspace{-0.8em}
				\py{pytex_fig('scripts/map.py', conf='slides/template.conf')}
			\end{frame}
			\begin{frame}{Statistic Map --- centered on largest cluster}
				\vspace{-0.8em}
				\py{pytex_fig('scripts/map_.py', conf='slides/template.conf')}
			\end{frame}
			\begin{frame}{Significance Thresholding}
				\begin{center}
					\textcolor{lg}{Multiple Comparison problem:}

					$\{H_{0,0}, ..., H_{0,99}\}  \Rightarrow$ 5 false rejections (type 1 rrors) with $p\leq0.05$
				\end{center}
				\begin{itemize}
					\item FWER - Bonferroni correction:\\
					\begin{center}\scriptsize \textcolor{lg}{$\alpha = $ probability of rejecting at least one true $H_{0,i}$}\end{center}
					\begin{center} $p_i \leq\frac{\alpha}{m}$ ; e.g. $\frac{0.05}{100} = 0.0005$\end{center}
					\item FDR - e.g. Benjamini–Hochberg procedure:\\
					\begin{center}\scriptsize \textcolor{lg}{$\alpha = $ proportion of rejected $H_{0,i}$ which were true}\end{center}
					\begin{center} $p_i \leq\frac{\alpha(m+1)}{2m}$ ; e.g. $\frac{0.05(100+1)}{2\cdot100} = 0.02525$\end{center}
				\end{itemize}
			\end{frame}
	\section{References}
		\scriptsize
		\bibliographystyle{unsrt}
		\bibliography{./bib}

\end{document}
