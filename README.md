# The Document

This is a reproducible document based on [RepSeP](https://github.com/TheChymera/RepSeP) (video intro to the RepSeP technology [may be seen here](https://youtu.be/WbjQYBuyKdk)).
Provided the dependencies specified in the [appropriate file](.gentoo/app-text/repsep_efa/repsep_efa-99999.ebuild) are made available, the document can be compiled from scratch by running `./compile.sh` from its root.  

# Materials for the practical day of the course:

Download and install:

* MRIcroGL or FSLeyes
* FSL or nibabel (first is operated via Bash, second via Python)

If you use windows:

* Gitbash ( https://gitforwindows.org/ )

Also download:

* chymera.eu/distfiles/mouse-brain-atlases-0.3.tar.xz
* chymera.eu/distfiles/samri_edudata.tar.xz
